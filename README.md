# fastrepo

> Create new python projects in minutes

## Quick start

`fastrepo` is a command line tool to quickly create new python project repositories.

### Instalation

- First install the package using `pip`.
 
```shell
pip install --user fastrepo
```

> [!NOTE]
> It is recommended to install the package in user space (using `--user`) so that no permission error is raised.

Once the package is installed, you can use it to create a new repo:

```shell
python -m fastrepo
```

## Documentation

Read the documentation at <https://gu-charbon.gitbook.io/fastrepo>