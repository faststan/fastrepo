### Feature
* Added semantic-release publish support through setup.cfg configuration ([`5fb4fdc`](https://gitlab.com/faststan/fastrepo/-/commit/5fb4fdc335b51354ad35d7ceafc0f4e04e8cf743))
